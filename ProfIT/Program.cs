﻿using ProfIT;

Generator generator = new Generator();
CancellationToken cancellationToken = new CancellationToken();

AverageConsumer consumer = new AverageConsumer();


generator.Start(consumer.OnTagCreated, cancellationToken);
