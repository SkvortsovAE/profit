﻿namespace ProfIT
{
    internal class Generator
    {
        Random random = new Random();
        private object lockObject = new object();
        private void GenerateTags(int milliseconds, int id, Action<Tag> action, CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                DateTimeOffset timestamp = DateTimeOffset.UtcNow;
                int value = random.Next(1, 101);
                var createdTag = new Tag(timestamp, value, id);
                action.Invoke(createdTag);
                lock (lockObject)
                {
                    using (StreamWriter writer = new StreamWriter(@"log1.txt", true))
                    {
                        writer.WriteLine(createdTag.ToString());
                    }
                    Monitor.Pulse(lockObject);
                }

                Thread.Sleep(milliseconds);
            }
        }
        public void Start(Action<Tag> action, CancellationToken cancellationToken)
        {
            var task1 = Task.Run(() => GenerateTags(100, 1, action, cancellationToken));
            var task2 = Task.Run(() => GenerateTags(7, 2, action, cancellationToken));
            var task3 = Task.Run(() => GenerateTags(3, 3, action, cancellationToken));

            task1.Wait();
            task2.Wait();
            task3.Wait();
        }
    }
}
