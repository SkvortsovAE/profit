﻿using System.Collections.Concurrent;
using System.Text;

namespace ProfIT
{
    internal class AverageConsumer
    {
        private LinkedList<Tag> tags1 = new LinkedList<Tag>();
        private LinkedList<Tag> tags2 = new LinkedList<Tag>();
        private LinkedList<Tag> tags3 = new LinkedList<Tag>();
        private ConcurrentDictionary<int, double> averages = new ConcurrentDictionary<int, double>();
        private object lockObject = new object();
        private Timer timer;
        public AverageConsumer()
        {
            TimerCallback timerCallback = new TimerCallback(DisplayAverages);
            timer = new Timer(timerCallback, null, 1000, 1000);
        }

        private void DisplayAverages(object obj)
        {
            StringBuilder builder = new StringBuilder();
            averages.TryGetValue(1, out double firstValue);
            averages.TryGetValue(2, out double secondValue);
            averages.TryGetValue(3, out double thirdValue);
            builder.AppendLine("---------------------------------------------------------------------");
            builder.AppendLine($"FirstAverage = {firstValue}");
            builder.AppendLine($"SecondAverage = {secondValue}");
            builder.AppendLine($"ThirdAverage = {thirdValue}");
            builder.AppendLine("---------------------------------------------------------------------");

            var result = builder.ToString();

            Console.WriteLine(result);

            lock (lockObject)
            {
                using (StreamWriter writer = new StreamWriter(@"log2.txt", true))
                {
                    writer.WriteLine(result);
                }
                Monitor.Pulse(lockObject);
            }
        }
        public void OnTagCreated(Tag tag)
        {
            switch (tag.ID)
            {
                case 1:
                    ChangeListOfTags(tags1, tag);
                    FillAverages(tags1, tag);
                    break;
                case 2:
                    ChangeListOfTags(tags2, tag);
                    FillAverages(tags2, tag);
                    break;
                case 3:
                    ChangeListOfTags(tags3, tag);
                    FillAverages(tags3, tag);
                    break;
                default:
                    break;
            }
        }

        private double CalculateAverage(LinkedList<Tag> list)
        {
            return list.Average(x => x.Value);
        }

        private void ChangeListOfTags(LinkedList<Tag> list, Tag tag)
        {
            list.AddLast(tag);
            if (list.Count > 10)
            {
                list.RemoveFirst();
            }
        }

        private void FillAverages(LinkedList<Tag> list, Tag tag)
        {
            var avg = CalculateAverage(list);
            averages.AddOrUpdate(tag.ID, avg, (key, oldValue) => avg);
        }
    }
}
