﻿namespace ProfIT
{
    internal class Tag
    {
        public Tag(DateTimeOffset timestamp, int value, int id)
        {
            Timestamp = timestamp;
            Value = value;
            ID = id;
        }

        public DateTimeOffset Timestamp { get; set; }
        public int ID { get; set; }
        public int Value { get; set; }

        public override string ToString()
        {
            return $"Tag(timestamp={Timestamp}, id={ID}, value={Value})\n";
        }
    }
}
